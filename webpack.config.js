var LiveReloadPlugin = require('webpack-livereload-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: "./entry.js",
	output: {
		path: __dirname+"/public/assets",
		publicPath: "assets/",
		filename: "bundle.js"
	},
	module: {
		loaders: [
			{
				test: /\.css$/,
				loader: "style-loader!css-loader"
			},
			{
				test : /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
				loader : 'file-loader'
			},
			{
				test: /\.ejs$/,
				loader: "ejs-compiled-loader"
			}
		]
	},
	plugins: [
		new LiveReloadPlugin({appendScriptTag: true}),
		new HtmlWebpackPlugin({
			filename: '../index.html',
			template: 'index.ejs',
			inject: false
		})
	],
	target: "node-webkit"
};
