module.exports = function(app){
	app.directive('menuEntry', ['$animate', function($animate){
		return {
			link: function(scope, element, attrs) {
				if(scope.menu == undefined){
					scope.menu = {
						items: [],
						change(id){
							scope.menu.selected = id;
						},
						add: function(name, icon, id){
							scope.menu.items.push({
								name: name,
								icon: icon,
								id: id
							});

							if(scope.menu.items.length == 1){
								scope.menu.selected = id;
							}
						}
					};
				}

				scope.menu.add(attrs.menuName, attrs.menuIcon, attrs.menuId);

				scope.$watch('menu.selected', function(value) {
					$animate[value == attrs.menuId ? 'removeClass' : 'addClass'](element, "ng-hide", {
						tempClasses: "ng-hide"
					});
				});
			}
		}
	}]);
}
