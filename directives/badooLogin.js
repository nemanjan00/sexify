// NodeWebkit

var gui = require("nw.gui");
var Window = gui.Window.get();

module.exports = function(app){
	app.directive('badooLogin', [function(){
		return {
			scope: {
				badooLogin: '&'
			},
			link: function(scope, element, attrs){
				element[0].setAttribute("nwdisable", "")
				element[0].setAttribute("nwfaketop", "")

				element[0].src = "https://badoo.com/";

				element.on('load', function(){
					var send = function(origin){
						var listener = function(){
							if(B.Session.getIsAuth()){
								var session = {
									cookie: document.cookie,
									sessionId: B.Session.getSessionId(),
									sessionUserId: B.Session.getUserId()
								};

								respond(session);
								clearInterval(interval);
							}
						}

						var interval = setInterval(listener, interval);
					};


					element[0].contentWindow.respond = function(data){
						return scope.badooLogin({session: data});
					};

					var code = "var send = "+send+"; send()";

					Window.eval(element[0], code);
				})
			}
		}
	}]);
}
