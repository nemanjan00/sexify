// Hack for livereload

// require("./index.ejs");

// Styles

require('photon/dist/css/photon.css');
require('./style.css');

// Angular & Jquery

require('angular');
require('jquery');


// App

var app = angular.module('top.nemanja.sexify', []);

require("./controllers")(app);
