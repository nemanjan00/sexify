module.exports = function(app){
	require("../directives/badooLogin")(app);
	require("../directives/menuEntry")(app);

	app.controller("SexifyController", function($scope){
		$scope.safeApply = function(fn) {
			var phase = this.$root.$$phase;
			if(phase == '$apply' || phase == '$digest') {
				if(fn && (typeof(fn) === 'function')) {
					fn();
				}
			} else {
				this.$apply(fn);
			}
		};

		$scope.badoo = {
			status: false,
			session: "",
			login: function(data){
				$scope.safeApply(function() {
					$scope.badoo.status =  true;
					$scope.badoo.session = data;
				});
			}
		};
	});
};

